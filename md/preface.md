<!--
Copyright © 2019 by Pongsak Suvanpong All Rights Reserved (psksvp@gmail.com)

License:
 
Creative Commons Non-Commercial Share Alike 4.0
https://creativecommons.org/licenses/by-nc-nd/4.0/legalcode
-->


# Preface

This story is my imagination run wild after reading the [Condor Trilogy](https://en.wikipedia.org/wiki/Condor_Trilogy) novels by [Jin Yong](https://en.wikipedia.org/wiki/Jin_Yong). There are three novels in the trilogy. I love all three of them, but the one that captivates me the most is the [Heavenly Sword and Dragon Saber](https://en.wikipedia.org/wiki/The_Heaven_Sword_and_Dragon_Saber) (HSDS). 

I have never written a story before, but I have read many in wide variety of genres. The [wuxia](https://en.wikipedia.org/wiki/Wuxia) genre is one that I like, though only Jin Yong novels that I have read. I have tried other wuxia authors, but they somehow cannot keep me reading until the end like those from Jin Yong.

The main reason that I am writing is to satisfy myself. I feel I do not have enough. I want more, I want the story to continue. Even if [JinYong](https://www.straitstimes.com/asia/east-asia/famed-chinese-martial-arts-novelist-jin-yong-dies-aged-94-hong-kong-media) were still alive, he would not be writhing any more of the trilogy. 
 
Most of this writing is unedited. There are grammatical, spelling errors and wrong words used. If you see any errors, please email me (psksvp@gmail.com). Please also let me know if you like or hate it.


## Warning

Most of this writing is unedited. There are grammatical, spelling errors and wrong words used. I am trying my best to fix any error before I commit, however it is hard to put myself in a reader eyes. 

If you see any errors, please email me (psksvp@gmail.com). Please also let me know if you like or hate it. I can also be reached through [facebook page (https://www.facebook.com/hsdsMe/)](https://www.facebook.com/hsdsMe/). I will announce the availability of a new chapter in the facebook page.

This novel is still under heavy development. Any chapter that is marked with **Unfinished** might get plot changed in each update that I commit.

## How to Read

The novel should be read in sequential order from the lower to the higher chapter number. Each chapter has its conclusion, but many clues and characters are connected from chapter 41. Before reading, please also read the precursor. 
