# On The Grassland

> On The Grassland is a continuing story from the end of JinYong's novel The Heavenly Sword and Dragon Saber  

Copyright © 2019 by Pongsak Suvanpong All Rights Reserved (psksvp@gmail.com)

# Plot 

I based my story from the novel to continue the story. This story line branched out from the novel right after the event at Shaolin temple [but before our main characters; Zhang Wuji and Zhao Min were drugged by Zhu Yuanzhang](https://wuxiasociety.com/the-heaven-sword-and-the-dragon-sabre-chapter-40/14/).  

At Haozhou, Zhao Min felt that if she stayed by Zhang Wuji side, she would cause the conflict of interest between Zhang Wuji and his subordinates. Though she wanted Zhang Wuji to leave the post as the leader of the Ming Cult and followed her, but she did not want to force him. She decided to leave and asked Zhang Wuji to come find her, when he was ready to do so. She moved herself out of the central plain to a far away place in western Mongolia. 

Our story started when Zhang Wuji found his lost love Zhao Min after almost 4 years apart. After having the sweetest time together, Zhang Wuji found out that Zhao Min wasn't just waiting for him. She still played a big role in the political arena in the Central Plains.


# Table of Contents

[Chapter 40.5 : A Lost Love Is Found.](https://psksvp.github.io/hsdsMe/mpt.html)

> Zhang Wuji replied, *"Min Min, I miss you with every breath I take"* and he cried out like a little baby.

[Chapter 41   : A Cactus Can Be Very Sweet.](https://psksvp.github.io/hsdsMe/mpt.html) 

> Zhao Min shouted while she was still running, *"Zhang Wuji! Let us stop kidding around now, the horses don't like it. My subordinates at the trading post could hear us!"*

[Chapter 42   : A New Doctor On The Grass Land.](https://psksvp.github.io/hsdsMe/mpt.html)

> Inside was a narrow passage with a stair. They walked down the stair, it was a cave. The surrounding wall of the cave was emitting soft green color light.

[Chapter 43   : The Invisible Living Things.](https://psksvp.github.io/hsdsMe/mpt.html)

> While Zhang Wuji was helping Zhao Min put on the soft armor, he was thinking that just half an hour ago, he was kissing and hugging the soft, gentle and lovable Zhao Min who wanted to have children with him, but now she had just tuned into a strong military commander who was going to face a possible tough challenge.

[Chapter 44   : The Cactus Has Ripened.](https://psksvp.github.io/hsdsMe/mpt.html)

> Zhang Wuji walked to Zhao Min and about to help her off from the horse. Suddenly Zhao Min fell down, but Zhang Wuji managed to catch her in his arm. *"MinMin, Are you all right? Are you ok? MinMin answer me please!"*, Zhang Wuji asked loudly.
  
[Chapter 45   : It Was The Devil's Own To Trust A Friend.](https://psksvp.github.io/hsdsMe/mpt.html)

> Zhang Wuji moved in front of Zhao Min to shield her from any danger. After a few moments, there was no movement from the person, he then walked to the person and picked the baby up then passed it to Zhao Min. He turned the person around and exclaimed loudly, *"Zhiruo....!"*.

[Chapter 46   : The Cactus Has Sprouted.](https://psksvp.github.io/hsdsMe/mpt.html)

> Zhao Min stood up and said, *"Hero Yang Xiao, you are a great man. I would say that heaven blessed me to have you herr......"* Zhao Min could not finish her speech, she felt dizzy and about to fall down.

[Chapter 47   : There Was A Wise Man In The Southern Land.](https://psksvp.github.io/hsdsMe/mpt.html)

> Lady Yang walked Zhang Wuji and Zhao Min into an underground room. Then she led them into a narrow hallway. Zhang Wuji had to bend down, so his head would not hit the ceiling. The hallway was very long. Once they got into a large hall, there were multi colors light emitting from a device. When Zhao Min saw the device, she exclaimed loudly, *"The Chang'An Cypher."* Zhang Wuji looked at Zhao Min with a puzzling face. Zhao Min then said, *"Master Zhuge has told me about it when I was 10 years old. I didn't think that it existed"*

[Chapter 48   : The Cactus Is Sweeter Than The Crystal Angel.](https://psksvp.github.io/hsdsMe/mpt.html)

> From the wounds, Zhang Wuji could see that Zhao Min had not tried to harm Yin Liting. She was just trying to disable him from using the Nine Yin White Bone Claw. Zhao Min had planed everything out even in a duel like this. She never used brute force to do anything. He was thinking that her sword skill had advanced so much. Even though, he had practiced with her very often, but seeing Zhao Min executed the movements in a real fight made him feel that Zhao Min martial-art was now better than all his martial uncles in many level. None of them could match Zhao Min's sword art and her crystal clear sharp mind. She could subdue them in less than 20 moves. 

[Chapter 49   : A Promise Is Not A Trust.. (unfinished)](https://psksvp.github.io/hsdsMe/mpt.html)

> Zhao Min did not know how long she had slept. When she opened her eyes again, she peeked outside to see that the sun was just above the horizon on the western sky. There was a person standing on a white sand dune about a Li away. Zhao Min was not sure who the person was or if she was in a dream. The person was quite tall with a very board chest and dressed in white, he was probably about 40 or 50 years old. The silver hair on his head was made into a taoist style bun. His face was long with the clean shaved. He was slowly walking toward the tent.   

[Chapter 50   : An Evil Is A Die Hard. (unfinished)](https://psksvp.github.io/hsdsMe/mpt.html)

> Emperor HongWu was busy getting dressed up, he shouted, *"Guard! Guard! Guard! There is an intruder."* The two concubines on the bed were still half naked, so they pulled the blankets to cover their body. When Emperor HangWu had a yellow robe to cover his body, but the guards still had not shown up. Then Emperor HongWu said with a scary voice, *"Who are you? Why are you here?"* The intruder replied, *"I am Emperor HongWu, who are you? Why are you here?"*
