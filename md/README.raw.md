# On The Grassland

> On The Grassland is a continuing story from the end of JinYong's novel The Heavenly Sword and Dragon Saber  

Copyright © 2019 by Pongsak Suvanpong All Rights Reserved (psksvp@gmail.com)

Version : [ VERSION ]
Date : [ DATE ]

![The Beautiful White Lake. The image was taken from https://en.wikipedia.org/wiki/Terkhiin_Tsagaan_Lake](./md/whiteLake.jpg)

# License

[Creative Commons Non-Commercial Share Alike 4.0](https://creativecommons.org/licenses/by-nc-nd/4.0/legalcode)

# News and Notes

[News and Notes](./md/news.md)

# Preface

This story is my imagination run wild after reading the [Condor Trilogy](https://en.wikipedia.org/wiki/Condor_Trilogy) novels by [Jin Yong](https://en.wikipedia.org/wiki/Jin_Yong). There are three novels in the trilogy. I love all three of them, but the one that captivates me the most is the [Heavenly Sword and Dragon Saber](https://en.wikipedia.org/wiki/The_Heaven_Sword_and_Dragon_Saber) (HSDS). [The unofficial English translation of the trilogy can be found here in epub format.](https://www.dropbox.com/sh/ugsorvumyo0mh8r/AABstfKrA5wvuDrPjzf3JKzxa?dl=0) The translation was done by netizens whose names were in the books to their credits.  

I have never written a story before, but I have read many in wide variety of genres. The [wuxia](https://en.wikipedia.org/wiki/Wuxia) genre is one that I like, though only Jin Yong novels that I have read. I have tried other wuxia authors, but they somehow cannot keep me reading until the end like those from Jin Yong.

The main reason that I am writing is to satisfy myself. I feel I do not have enough. I want more, I want the story to continue. Even [if JinYong were still alive](https://www.straitstimes.com/asia/east-asia/famed-chinese-martial-arts-novelist-jin-yong-dies-aged-94-hong-kong-media) , he would not be writing any more of the trilogy. 

# Warning

Most of this writing is unedited. There are grammatical, spelling errors and wrong words used. I am trying my best to fix any error before I commit, however it is hard to put myself in a reader eyes.


If you see any errors, please email me (psksvp@gmail.com). Please also let me know if you like or hate it. I can also be reached through [facebook page (https://www.facebook.com/hsdsMe/)](https://www.facebook.com/hsdsMe/). I will announce the availability of a new chapter in the facebook page.

This novel is still under heavy development. Any chapter that is marked with **Unfinished** might get plot changed in each update that I commit.

# How to Read

The novel should be read in sequential order from the lower to the higher chapter number. Each chapter has its conclusion, but many clues and characters are connected from chapter 41. Before reading, please also read the [Precursor section](#precursor). 

1. Reading Online; There are two ways to the read online.

    * [Properly Rendered Webpages](https://psksvp.github.io/hsdsMe/index.html) : The contents in the site are always in sync with the raw Markdown files. However when I commit the updates to github, the content on the site will be updated in about 5 minutes later than the raw Markdown files, the epub and pdf (please see below).

    * [Raw Markdown](#table-of-contents) : You can also read online from the *raw Markdown* files which github renders, but it is not pretty and can be hard to navigate between chapters. To read online this way, please scroll down to the section [Table of Contents](#table-of-contents). 

2. Downloads for Offline Reading

    You can download ebook in **epub** or **pdf** to read offline. They are always in sync with the online version. The Current Version is **VERSION**, published on **DATE** .
    
    [Click here to download ebook files](https://psksvp.github.io/hsdsMe/download.html)

    The ebooks have been produced by a great free software; [pandoc](https://pandoc.org/). The manuscript for this novel was prepared using [Markdown](https://www.markdownguide.org/) markup language.

# Precursor 

I heavily used the events from the novel to continue the story. This story line branched out from the novel right after the event at Shaolin temple [but before our main characters; Zhang Wuji and Zhao Min were drugged by Zhu Yuanzhang](https://wuxiasociety.com/the-heaven-sword-and-the-dragon-sabre-chapter-40/14/).  

At Haozhou, Zhao Min felt that if she stayed by Zhang Wuji side, she would cause the conflict of interest between Zhang Wuji and his subordinates. Though she wanted Zhang Wuji to leave the post as the leader of the Ming Cult and followed her, but she did not want to force him. She decided to leave and asked Zhang Wuji to come find her, when he was ready to do so. She moved herself out of the central plain to a far away place in western Mongolia. 

Our story started when Zhang Wuji found his lost love Zhao Min after 4 years apart. 

# Table of Contents

[Chapter 40.5 : A Lost Love Is Found.](./md/ch405And41.md)

> Zhang Wuji replied, *"Min Min, I miss you with every breath I take"* and he cried out like a little baby.

[Chapter 41   : A Cactus Can Be Very Sweet.](./md/ch405And41.md) 

> Zhao Min shouted while she was still running, *"Zhang Wuji! Let us stop kidding around now, the horses don't like it. My subordinates at the trading post could hear us!"*

[Chapter 42   : A New Doctor On The Grass Land.](./md/ch42.md)

> Inside was a narrow passage with a stair. They walked down the stair, it was a cave. The surrounding wall of the cave was emitting soft green color light.

[Chapter 43   : The Invisible Living Things.](./md/ch43.md)

> While Zhang Wuji was helping Zhao Min put on the soft armor, he was thinking that just half an hour ago, he was kissing and hugging the soft, gentle and lovable Zhao Min who wanted to have children with him, but now she had just tuned into a strong military commander who was going to face a possible tough challenge.

[Chapter 44   : The Cactus Has Ripened.](./md/ch44.md)

> Zhang Wuji walked to Zhao Min and about to help her off from the horse. Suddenly Zhao Min fell down, but Zhang Wuji managed to catch her in his arm. *"MinMin, Are you all right? Are you ok? MinMin answer me please!"*, Zhang Wuji asked loudly.
  
[Chapter 45   : It Was The Devil's Own To Trust A Friend.](./md/ch45.md)

> Zhang Wuji moved in front of Zhao Min to shield her from any danger. After a few moments, there was no movement from the person, he then walked to the person and picked the baby up then passed it to Zhao Min. He turned the person around and exclaimed loudly, *"Zhiruo....!"*.

[Chapter 46   : The Cactus Has Sprouted.](./md/ch46.md)

> Zhao Min stood up and said, *"Hero Yang Xiao, you are a great man. I would say that heaven blessed me to have you herr......"* Zhao Min could not finish her speech, she felt dizzy and about to fall down.

[Chapter 47   : There Was A Wise Man In The Southern Land.](./md/ch47.md)

> Lady Yang walked Zhang Wuji and Zhao Min into an underground room. Then she led them into a narrow hallway. Zhang Wuji had to bend down, so his head would not hit the ceiling. The hallway was very long. Once they got into a large hall, there were multi colors light emitting from a device. When Zhao Min saw the device, she exclaimed loudly, *"The Chang'An Cypher."* Zhang Wuji looked at Zhao Min with a puzzling face. Zhao Min then said, *"Master Zhuge has told me about it when I was 10 years old. I didn't think that it existed"*

[Chapter 48   : The Cactus Is Sweeter Than The Crystal Angel.](./md/ch48.md)

> From the wounds, Zhang Wuji could see that Zhao Min had not tried to harm Yin Liting. She was just trying to disable him from using the Nine Yin White Bone Claw. Zhao Min had planed everything out even in a duel like this. She never used brute force to do anything. He was thinking that her sword skill had advanced so much. Even though, he had practiced with her very often, but seeing Zhao Min executed the movements in a real fight made him feel that Zhao Min martial-art was now better than all his martial uncles in many level. None of them could match Zhao Min's sword art and her crystal clear sharp mind. She could subdue them in less than 20 moves. 

[Chapter 49   : A Promise Is Not A Trust.](./md/ch49.md)

> Zhao Min did not know how long she had slept. When she opened her eyes again, she peeked outside to see that the sun was just above the horizon on the western sky. There was a person standing on a white sand dune about a Li away. Zhao Min was not sure who the person was or if she was in a dream. The person was quite tall with a very board chest and dressed in white, he was probably about 40 or 50 years old. The silver hair on his head was made into a taoist style bun. His face was long with the clean shaved. He was slowly walking toward the tent.   

[Chapter 50   : An Evil Is A Die Hard. (unfinished)](./md/ch50.md)

> Wan MaSun replied, *"I have just been authorized by the emperor to take 30,000 men to eradicate all the major sects once and for all."* He smiled arrogantly and continued, *"The first target is the old Yang Xiao and his followers. Did you say that the woman surname Zhao and Zhou were with that Yang Xiao at a Trading Post in the TarTar land? I want you to take me there."* Then he walked to gently tap on Yin Liting's shoulder, *"You will provide a great service to the emperor. He will shower you with great power and gifts."* Then Wan MaSun put the small jar on the table. 


[Chapter 51 : A New Face Of The Ruler.(unfinished)](./md/ch51.md)

> Emperor HongWu was busy getting dressed up, he shouted, *"Guard! Guard! Guard! There is an intruder."* The two concubines on the bed were still half naked, so they pulled the blankets to cover their body. When Emperor HangWu had a yellow robe to cover his body, but the guards still had not shown up. Then Emperor HongWu said with a scary voice, *"Who are you? Why are you here?"* The intruder replied, *"I am Emperor HongWu, who are you? Why are you here?"*

[Terms](./md/terms.md)
