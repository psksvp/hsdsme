# On The Grassland (Reading Online)

> On The Grassland is a continuing story from the end of JinYong's novel The Heavenly Sword and Dragon Saber  

Copyright © 2019 by Pongsak Suvanpong All Rights Reserved (psksvp@gmail.com)


<img src="media/cover.png" height="300" />

## [Big Screen Device Version](mpt.html).

This version is suitable for device with large screen like your laptop/desktop or tablet.

          ____
         ||""||
         ||__||
         [ -=.]`)
         ====== 0   

## [Small Screen Device Version](spt.html)

This version is suitable for device with small screen. 

       ___i
      |---|    
      |[_]|    
      |:::|    
      |:::|    
      `\   \   
        \_=_\ 
        
        

*ascii art by jgs*
