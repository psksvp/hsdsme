# Terms

## Names (Jinyong)

* Zhang Wuji == Zhang Jiaozhu
* Zhao Min == Shaomin == Min min Temür == Min Mie == MinMin 
* Zhou Zhiruo == Emei Jiaomen
* Zhang Sanfeng ==  Zhang Zhenren (Tao monk surname Zhang)
* Xiao Zhao
* Han Lin'Er 
* Fan Yao == Ku DaShi == Kutoutuo
* Yin Li == Zhu'er (spider) == Yin Biaomei (cousin surname Yin)
* Wang Chongyang
* Wei Yixiao == The dirty bat
* Ding Minjun == 
* Yin Liting == 
* Li Sicui == One of the Divine Arrow Eight
* Zhou Wushu == One of the Divine Arrow Eight 
* Miejue Shitai == was the Emei's Jiaomen 
* Yin Yiwang == A pervert 
* Zhu Yuanzhang == The demon surname Zhu == HongWu Emperor.

## Names (new)

* Zhao Kuangyin == Zhang Wuji and Zhao Min's son
* Zhang Yulian == Zhang Wuji and Zhao Min's daughter
* Yang Nousha == Yang Buhui's daughter
* Li Han == Yang Buhui's older son
* Duan Zhengkang == A wise man from Dali
* Minoo == A Persian who married Zhou Wushu
* Nima == Minoo's daughter
* Omar == A Persian man who is hard to die
* Eyman == A tough Turkic man == The Turkic's chief  
* Domenico == The teacher 
* Agnolo == Papa's protector knight from Vaticano
* Bartolomeo == Papa's protector knight from Vaticano
* Corelia == Papa's protector knight from Vaticano
* Drusia == Papa's protector knight from Vaticano
* Damela == An only daughter of Toma
* Hynum == Jamusha's son
* Jamusha == Chief of the Kucha
* Toma == Chief of the Tuyuhun
* Zhuge Zhi == Master Zhuge == The direct descendent of [Zhuge Liang](https://en.wikipedia.org/wiki/Zhuge_Liang) 
* Yunnan Boy == The person eunuch to Zhu Yuanzhang 
* Wan MaSun == The head of the Special Investigation Bureau of the Ming Government.
* Liu Tien == A GaiBang Branch Master
* Liu Biyu == Liu Tien's wife
* 527 == Zhao Min's subordinate, the head of XiangYang region spy operation. 
* XiaoMei == 527's subordinate
* 526 == Zhao Min's subordinate, the head of Luoyang region spy operation. She used to be Zhao Min's mother personal body guard.
* Harendra == An Indus doctor who was a good friend of Wei Yixiao.
* Jandra == Harendra's sworn bother

## Places

* [Central Plain](https://en.wikipedia.org/wiki/Zhongyuan) 
* Zeng AhNuo == Zhang Wuji's AKA == The trading post in this story
* Dadou == modern day Beijing
* Haozhou == a city in Central Plain 
* XiangYang == a city in Central Plain
* Chang'An == Xi'An == a city in Central Plain
* Lin'An == a city in Central Plain
* Luoyang == a city in Central Plain
* Pyu == a kingdom state (modern day Burma)
* Khmer == a kingdom in SEA (modern day Thailand, Laos and Cambodia)
* Mount Zhongnan

## Sects

* [Shaolin Monastery](https://en.wikipedia.org/wiki/Shaolin_Monastery) == Shàolín sì
* Emei
* WuDang
* GaiBang  == [Beggars' Sect](https://en.wikipedia.org/wiki/Beggars%27_Sect)
* Ancient Tomb Sect
* Quanzhen Sect


## Tribes

* Turkic == nomadic tribe in central asia
* Tuyuhun == nomadic tribe in central asia
* Kucha == nomadic tribe in central asia


## Callings

* BangZhu == GaiBang Leader
* Jiaozhu == Cult Leader
* Jiaomen == Sect Leader
* Junzhu == Princess
* Shifu == A master who is also a teacher
* BoBo == Uncle == 伯伯
* Ge == 哥
* GeGe == Gēgē == 哥哥
* GongZhi == MR.
* GūNiáng == GuNiang == Miss
* Xiao Seng == little person == A person of a lower status

## Misc

* kowtou == 
* Jianghu  == martial art world? == wulin?
* WuGong == Chi == internal energy == AKA "Luke! Use the force"
* QingGong == lightness skill
* Qian Kun Da Nuo Yi == Universal Grand Shift.
* Li == Ancient Distance Measurement unit in the Central plain.
* Koumiss == A type of alcoholic drink made from fermented goat or horse milk.
* chivalry ethics == code of conduct of the martial art world
* [sugar monkey](https://en.wikipedia.org/wiki/Sugar_sculpture)



