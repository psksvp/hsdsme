# News And Notes

## Sept Fri 13th, 2019

Since I have some unexpected problem with a file corruption last night, I have been doing some house keeping work this evening to make sure I won't lose my files in the future.

Then I went to look at the access stat. The largest group of readers who spend most time reading my novel are in Ipoh Malaysia.

I am really overwhelmed with surprises. I have never expected that anyone would want to read what I wrote. Most of the writing were what were in my mind. I write when I am on a train, bus, plane, at lunch or before sleep.

Again, I don't know if everyone who are reading my novel can see this post, I want to thank all of you.

![Readers from MY spend most time reading](./md/readersStat.png)


## Sept 11, 2019

I have finished Chapter 49. It took longer than I thought. Though, from the stat, many of you have been reading as I write. Anyway thank you very much for reading. Please let me know if you like or hate it by Emailing me [psksvp@gmail.com](psksvp@gmail.com) or going to the facebook page of this novel [https://www.facebook.com/hsdsMe/](https://www.facebook.com/hsdsMe/) 


## Sept 5, 2019

Hello All

From the github access stat, I can see that there are readers who follow me closely as I write. Access to read chapter 49 is the highest. Thank you very much. I have never thought that anyone would want to read what was in my head. ;) 

Anyway, after a long flight from *Chek Lap Kok*, I have managed to mess up the sequence of sections of chapter 49 when I committed the contents yesterday. I have just fixed it in the commit today. 

The sequence should be below.

* The Jianghu Case
* The Three Hundred
* The More Confident Game
* The Domino

Anyway, I hope that I will finish chapter 49 by early next week. 


## Aug 25, 2019

I will be going to Japan for a few days. My day work is a bit busy at the moment, so ch49 will be delay. I hope the delay won't stop your appetites for reading my wild imagination. ;)

PS. Sydney Airport is quiet tonight.


## Aug 24, 2019

Yes, I do love the Zhao Min character. So a music video about Zhao Min using the song I feel, is her. (Not the actress, though she played Zhao Min very well.)

[![Music Video Thumb](http://img.youtube.com/vi/Dts5chHaGxY/0.jpg)](http://www.youtube.com/watch?v=Dts5chHaGxY)

## Aug 23, 2019

I cried a little as I wrote the [ch49:*The Non Repeating History*](#the-non-repeating-history) 😢

## Aug 9, 2019

[Chapter 48](#chapter-48-the-cactus-is-sweeter-than-the-crystal-angel.) is finished. Please let me know if you hate or like it. psksvp@gmail.com

## July 18, 2019

I am starting on [Chapter 48](#chapter-48-the-cactus-is-sweeter-than-the-crystal-angel.) I have not had time to write much (as much as I'd like to). However, once I am back to my normal timetable, there should be content added everyday. ;)

## July 8, 2019

I thought I would not be able to finish [Chapter 47](#chapter-47-there-was-a-wise-man-in-the-southern-land.) until next week, because of traveling. But I flew out on Friday night, so I got two full days to finish. If you are reading, please let me know if you hate or like it.


## July 4, 2019

I thought I was going to finish [Chapter 47](#chapter-47-there-was-a-wise-man-in-the-southern-land.) this weekend, but I will travel to the land of the rising sun again. So look like I will not finish it, but the 9 hours flight will give me some time to write I hope, if the flight is not too pack. ;) The plot went in about 90% already.

Chapter 47 is a bit long, because there are two and a half story lines going concurrently. But I hope it will be very fun. There are many clues for the next Chapter. Below is the teaser
    
## June 26, 2019

I have just taken a look at the stat on my github (screen cap attached with this post). It surprised me. [Chapter 47](#chapter-47-there-was-a-wise-man-in-the-southern-land.) which is still on going was the most popular content. Currently chapter 47 is about 80% finished. Though, there could still be plot changes. It seems, there are regular readers. Thank you whoever you are.

![git popular chapter image](./md/gitPopCont.png)
    

## June, 14, 2019

I have finished [chapter 46](#chapter-46-the-cactus-has-sprouted). There are going more sentences and wording fixes coming, but there will be no plot changes. Please let me know if you like or hate it.    
