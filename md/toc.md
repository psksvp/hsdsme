<!--
Copyright © 2019 by Pongsak Suvanpong All Rights Reserved (psksvp@gmail.com)

License:
 
Creative Commons Non-Commercial Share Alike 4.0
https://creativecommons.org/licenses/by-nc-nd/4.0/legalcode
-->

# Table of Contents

[Chapter 40.5 : A Lost Love Is Found.](#chapter-40.5-a-lost-love-is-found.)

> Zhang Wuji replied, *"Min Min, I miss you with every breath I take"* and he cried out like a little baby.

[Chapter 41   : A Cactus Can Be Very Sweet.](#chapter-41-a-cactus-can-be-very-sweet.) 

> Zhao Min shouted while she was still running, *"Zhang Wuji! Let us stop kidding around now, the horses don't like it. My subordinates at the trading post could hear us!"*

[Chapter 42   : A New Doctor On The Grass Land.](#chapter-42-a-new-doctor-on-the-grassland.)

> Inside was a narrow passage with a stair. They walked down the stair, it was a cave. The surrounding wall of the cave was emitting soft green color light.

[Chapter 43   : The Invisible Living Things.](#chapter-43-the-invisible-living-things.)

> While Zhang Wuji was helping Zhao Min put on the soft armor, he was thinking that just half an hour ago, he was kissing and hugging the soft, gentle and lovable Zhao Min who wanted to have children with him, but now she had just tuned into a strong military commander who was going to face a possible tough challenge.

[Chapter 44   : The Cactus Has Ripened.](#chapter-44-the-cactus-has-ripened)

> Zhang Wuji walked to Zhao Min and about to help her off from the horse. Suddenly Zhao Min fell down, but Zhang Wuji managed to catch her in his arm. *"MinMin, Are you all right? Are you ok? MinMin answer me please!"*, Zhang Wuji asked loudly.
  
[Chapter 45   : It Was The Devil's Own To Trust A Friend.](#chapter-45-it-was-the-devils-own-to-trust-a-friend)

> Zhang Wuji moved in front of Zhao Min to shield her from any danger. After a few moments, there was no movement from the person, he then walked to the person and picked the baby up then passed it to Zhao Min. He turned the person around and exclaimed loudly, *"Zhiruo....!"*.

[Chapter 46   : The Cactus Has Sprouted.](#chapter-46-the-cactus-has-sprouted)

> Zhao Min stood up and said, *"Hero Yang Xiao, you are a great man. I would say that heaven blessed me to have you herr......"* Zhao Min could not finish her speech, she felt dizzy and about to fall down.

[Chapter 47   : There Was A Wise Man In The Southern Land.](#chapter-47-there-was-a-wise-man-in-the-southern-land.)

> Lady Yang walked Zhang Wuji and Zhao Min into an underground room. Then she led them into a narrow hallway. Zhang Wuji had to bend down, so his head would not hit the ceiling. The hallway was very long. Once they got into a large hall, there were multi colors light emitting from a device. When Zhao Min saw the device, she exclaimed loudly, *"The Chang'An Cypher."* Zhang Wuji looked at Zhao Min with a puzzling face. Zhao Min then said, *"Master Zhuge has told me about it when I was 10 years old. I didn't think that it existed"*

[Chapter 48   : The Cactus Is Sweeter Than The Crystal Angel.](#chapter-48-the-cactus-is-sweeter-than-the-crystal-angel.)

> From the wounds, Zhang Wuji could see that Zhao Min had not tried to harm Yin Liting. She was just trying to disable him from using the Nine Yin White Bone Claw. Zhao Min had planed everything out even in a duel like this. She never used brute force to do anything. He was thinking that her sword skill had advanced so much. Even though, he had practiced with her very often, but seeing Zhao Min executed the movements in a real fight made him feel that Zhao Min martial-art was now better than all his martial uncles in many level. None of them could match Zhao Min's sword art and her crystal clear sharp mind. She could subdue them in less than 20 moves. 

[Chapter 49   : A Promise Is Not A Trust.](#chapter-49-a-promise-is-not-a-trust.)

> Zhao Min did not know how long she had slept. When she opened her eyes again, she peeked outside to see that the sun was just above the horizon on the western sky. There was a person standing on a white sand dune about a Li away. Zhao Min was not sure who the person was or if she was in a dream. The person was quite tall with a very board chest and dressed in white, he was probably about 40 or 50 years old. The silver hair on his head was made into a taoist style bun. His face was long with the clean shaved. He was slowly walking toward the tent.

[Chapter 50   : An Evil Is A Die Hard. (unfinished)](#chapter-50-an-evil-is-a-die-hard.)

> Wan MaSun replied, *"I have just been authorized by the emperor to take 30,000 men to eradicate all the major sects once and for all."* He smiled arrogantly and continued, *"The first target is the old Yang Xiao and his followers. Did you say that the woman surname Zhao and Zhou were with that Yang Xiao at a Trading Post in the TarTar land? I want you to take me there."* Then he walked to gently tap on Yin Liting's shoulder, *"You will provide a great service to the emperor. He will shower you with great power and gifts."* Then Wan MaSun put the small jar on the table. 

[Chapter 51 : A New Face Of The Ruler.(unfinished)](#chapter-51-a-new-face-of-the-ruler.)

> Emperor HongWu was busy getting dressed up, he shouted, *"Guard! Guard! Guard! There is an intruder."* The two concubines on the bed were still half naked, so they pulled the blankets to cover their body. When Emperor HangWu had a yellow robe to cover his body, but the guards still had not shown up. Then Emperor HongWu said with a scary voice, *"Who are you? Why are you here?"* The intruder replied, *"I am Emperor HongWu, who are you? Why are you here?"*


[Terms](#terms) 

---
