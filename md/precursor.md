<!--
Copyright © 2019 by Pongsak Suvanpong All Rights Reserved (psksvp@gmail.com)

License:
 
Creative Commons Non-Commercial Share Alike 4.0
https://creativecommons.org/licenses/by-nc-nd/4.0/legalcode
-->

# Precursor 

I heavily used the events from the novel to continue the story. This story line branched out from the novel right after the event at Shaolin temple [but before our main characters; Zhang Wuji and Zhao Min were drugged by Zhu Yuanzhang](https://wuxiasociety.com/the-heaven-sword-and-the-dragon-sabre-chapter-40/14/).  

At Haozhou, Zhao Min felt that if she stayed by Zhang Wuji side, she would cause the conflict of interest between Zhang Wuji and his subordinates. Though she wanted Zhang Wuji to leave the post as the leader of the Ming Cult and followed her, but she did not want to force him. She decided to leave and asked Zhang Wuji to come find her, when he was ready to do so. She moved herself out of the central plain to a far away place in western Mongolia. 

Our story started when Zhang Wuji found his lost love Zhao Min after 4 years apart. 
