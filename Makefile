MDS=./md/license.md \
    ./md/news.md \
    ./md/preface.md \
    ./md/precursor.md \
    ./md/toc.md \
    ./md/ch405And41.md \
    ./md/ch42.md \
    ./md/ch43.md \
    ./md/ch44.md \
    ./md/ch45.md \
    ./md/ch46.md \
    ./md/ch47.md \
    ./md/ch48.md \
    ./md/ch49.md \
    ./md/ch50.md \
		./md/ch51.md \
    ./md/terms.md
		
DATE := $(shell date)		

## yes all targets are PHONY

all: versioning epub pdf html online offline
	
versioning: 
	./Automate.scala versioning
	
commit: clean all offline
	git add *
	git commit -m "$(MSG) -> on $(DATE)"
	git push	 

commitUpdate: clean all offline
	git add -u
	git add *
	git commit -m "$(MSG) -> on $(DATE)"
	git push		
	 
epub: $(MDS)
	pandoc --epub-cover-image=./md/cover.png -o ./docs/offline/OnTheGlassLand.epub ./md/title.yaml ./md/version.md $(MDS)

pdf: $(MDS)
	pandoc --top-level-division=chapter --toc --pdf-engine=xelatex -o docs/offline/OnTheGlassLand.pdf  ./md/fontSpec.md ./md/title.md $(MDS)	
	
html: $(MDS)
	pandoc --toc --self-contained -t html5 --mathjax --metadata pagetitle="On The Grassland" -s --css ./md/foghorn.css -o docs/offline/OnTheGlassLand.html  ./md/title.md $(MDS)		

mobi: epub
	kindlegen -c1 docs/hsdsMe.epub > /dev/null

offline: 
	./Automate.scala offlinefiles

online: epub cleanOnline
	unzip ./docs/offline/OnTheGlassLand.epub -d ./docs
	mv ./docs/EPUB/* ./docs/.
	rm -rf ./docs/EPUB
	cp ./docs/nav.xhtml ./docs/spt.html
	./Automate.scala insertAnalytics ./docs/spt.html
	./Automate.scala toSite ./docs/nav.xhtml
	./Automate.scala insertAnalytics ./docs/text
	cp ./md/mpt.html ./docs/mpt.html
	cp ./md/foghorn.css ./docs/styles/stylesheet1.css
	pandoc md/readOnlineChoice.md -o docs/index.html
	./Automate.scala insertAnalytics ./docs/offline/OnTheGlassLand.html
	
				
cleanOnline:
	rm -rf ./docs/EPUB ./docs/media ./docs/META-INF ./docs/styles ./docs/text ./docs/nav.xhtml ./docs/mimetype ./docs/toc.ncx ./docs/content.opf
					
clean: cleanOnline
	rm -f docs/index.html
	rm -f docs/offline/*
	rm -f docs/mpt.html docs/spt.html
	
	
	
		
