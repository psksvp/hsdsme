#!/bin/sh
exec scala "$0" "$@"
!#

// created by psksvp@gmail.com on Jul 31, 2019
object Automate
{
  import sys.process._
  
  lazy val date = (Seq("date").!!).trim
  lazy val version = Integer.parseInt(readString("version.int").trim)
  
  lazy val analytics = """
  <head>
  <!-- Google Analytics -->
  <script>
  (function(i,s,o,g,r,a,m){i['GoogleAnalyticsObject']=r;i[r]=i[r]||function(){
  (i[r].q=i[r].q||[]).push(arguments)},i[r].l=1*new Date();a=s.createElement(o),
  m=s.getElementsByTagName(o)[0];a.async=1;a.src=g;m.parentNode.insertBefore(a,m)
  })(window,document,'script','https://www.google-analytics.com/analytics.js','ga');

  ga('create', 'UA-146161875-1', 'auto');
  ga('send', 'pageview');
  </script>
  <!-- End Google Analytics -->
  """
  
  def main(args:Array[String]):Unit =
  {
    if(args.length == 2 && "toSite" == args(0) && fileExists(args(1)))
      transformToSite(args(1))
    else if(args.length == 1 && "versioning" == args(0))
    {
      versioning()
      prepareOffline()
    }
    else if(args.length == 1 && "offlinefiles" == args(0))
    {
      writeOffline()
    }
    else if(args.length == 2 && "insertAnalytics" == args(0))
    {
      if(isDir(args(1)))
      {  
        for(f <- listFiles(args(1)))
        {
          insertAnalytics(f)
        }
      }
      else
        insertAnalytics(args(1))
      
    }
    else
      println("Usage: read the code if you want use it.")
  }
  
  def insertAnalytics(file:String):Unit = 
  {
    val out = readString(file).replace("<head>", analytics)
    writeString(out, file)
  }
  
  def prepareOffline():Unit=
  {
    val offlineURL = s"https://psksvp.github.io/hsdsMe/offline/OnTheGlassLand-${version.toString}"
    val htmlDownload = "./md/download.html"
    val dn = readString(htmlDownload).replaceAll("VERSION", version.toString)
                                     .replaceAll("DATE", date)
                                     .replaceAll("EPUB_URL", s"$offlineURL.epub")
                                     .replaceAll("PDF_URL", s"$offlineURL.pdf")
                                     .replaceAll("HTML_URL", s"$offlineURL.html")
                                     
    writeString(dn, "./docs/download.html")   
  }
  
  def writeOffline()
  {
    val pver = (version - 1).toString
    val offlineFile = "./docs/offline/OnTheGlassLand"
    moveFile(s"$offlineFile.pdf", s"$offlineFile-${pver}.pdf")
    moveFile(s"$offlineFile.epub", s"$offlineFile-${pver}.epub")
    moveFile(s"$offlineFile.html", s"$offlineFile-${pver}.html")
  }
  
  def transformToSite(tocFile:String):Unit=
  {
    val pat = """<a\s+(?:[^>]*?\s+)?href=(["'])(.*?)\1""".r
    val toc = readString(tocFile)
    val srPair = for(v <- pat.findAllIn(toc)) yield (v, s"""$v target="content" """)
    val output = searchAndReplace(srPair.toSet, toc)
    writeString(output, tocFile)
  }
  
  def searchAndReplace(p:Set[(String, String)], inString:String):String=
  {
    if(p.size > 0)
    {
      val pair = p.head
      val rs = inString.replaceAllLiterally(pair._1, pair._2)
      searchAndReplace(p.tail, rs)
    }
    else
      inString
  }
  
  def versioning():Unit = 
  {
    if(false == fileExists("version.int"))
      println("ERROR: version.int is missing")
    else
    {
      val files = Array(("./md/title.raw.yaml", "./md/title.yaml"),
                        ("./md/title.raw.md", "./md/title.md"),
                        ("./md/version.raw.md", "./md/version.md"),
                        ("./md/README.raw.md", "./README.md"))
                        
      for((in, out) <- files)
      {
        println(s"versioning $in to $out")
        versionFile(in, out)
      } 
      versionCoverImage("./md/cover.raw.png", "./md/cover.png")
      updateVersion("version.int")                  
    }
  }
  
  def versionCoverImage(imagePath:String, outputPath:String):Unit =
  {     
    Seq("convert", imagePath, "-background", "Khaki",
        s"label:'Version ${version.toString} $date'", "+swap",
        "-gravity", "Center", "-append", outputPath).!!         
  }
  
  def versionFile(inFilePath:String, outFilePath:String):Unit = 
  {
    val mds = readString(inFilePath).replaceAll("VERSION", version.toString)
                                    .replaceAll("DATE", date)
    
    writeString(mds, outFilePath)  
  }
  
  def updateVersion(versionFilePath:String):Unit =
  {
    writeString((version + 1).toString, versionFilePath) 
  }
  
  def replace(token:String, withString:String, inString:String):String = 
  {
    inString.replaceAll(token, withString)
  }
  
  def writeString(s:String, toFileAtPath:String):Unit =
  {
    import java.io.PrintWriter
    new PrintWriter(toFileAtPath)
    {
      write(s)
      close()
    }
  }

  def readString(fromFileAtPath:String):String =
  {
    import sys.process._
    Seq("cat", fromFileAtPath).!!
  }
  
  
  def fileExists(path:String):Boolean = new java.io.File(path).exists()
  
  def unzip(zipFile:String, outputDir:String=""):Unit=
  {
    if(0 == outputDir.length)
      Seq("unzip", zipFile).!!
    else
      Seq("unzip", zipFile, s"-d $outputDir").!!
  }
  
  def moveFile(from:String, to:String):Unit=
  {
    Seq("mv", from, to).!!
  }
  
  def listFiles(dir:String):Seq[String]=
  {
    for(p <- (Seq("ls", dir).!!).split("\n")) yield s"$dir/${p.trim()}"
  }
  
  def isDir(path:String):Boolean = new java.io.File(path).isDirectory()
}